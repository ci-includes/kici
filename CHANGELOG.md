# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).


## [v0.9.0] - 2024-11-29
## Fixed
- panelize_fab: fix pipeline warning
- Do not generate stencils when no SMD pads
- Ignore missing repeat layers
- Ignore missing default symbol table
- Revert "fix: Always show ERC/DRC report
- pcb_print: Restore colored via's

## Changed
- kibot: Version bump

## Added
- Add variables for SCH/PCB tags
- Add design report


## [v0.8.1] - 2024-10-21
## Fixed
- Remove duplicate files for exports

## Changed
- Strip prefixes from release archive
- OCD fix for date and rev variables


## [v0.8.0] - 2024-10-21
## Fixed
- Avoid duplicate (js) files on release
- Do not refill zones during pdf print

## Added
- Add support to export a kicad project
- Also release an unmodified archive of everything
- Add simple 3D model linter


## [v0.7.0] - 2024-10-04
## Changed
- Also generate a steel stincil alignment jig

## Added
- Add KiCanvas support


## [v0.6.2] - 2024-10-02
## Fixed
- CI: Trigger stencils from pipeline


## [v0.6.1] - 2024-09-30
## Fixed
- lfs: Do not track files


## [v0.6.0] - 2024-09-29
## Fixed
- kici: Encapsulate git variables
- doc: Fix documentation example

## Change
- eda: Add LCSC components
- fab: Reuse output path

## Added
- logo: Add placeholder logo
- bom: Enable more outputs
- KiCi: Add stencil output
- kici: Add report output
- kici: Add panelization output


## [v0.5.0] - 2024-06-15
## Change
- Upgrade to KiCad 8 and KiBot 1.7.0


## [v0.4.2] - 2024-02-21
## Fixed
- CI: Handle single project names better
- Ensure we store asset.json content
- Extract archives for publication


## [v0.4.1] - 2024-02-16
## Fixed
- Do not release navigate and present as files

## Added
- deploy: Store filelist map


## [v0.4.0] - 2024-02-14
### Fixed
- Add missing top-zoom level
- Support deploy on singular projects

### Removed
- Remove paste and adhesion from fab layers

### Added
- Publish to gitlab pages
- Include more pages views


## [v0.3.0] - 2024-02-02
### Fixed
- Move to KiBot version 1.6.4 from dev
- PDF Centering
- Run attach diff only for MR's


## [v0.2.0] - 2024-01-31
### Added
- Support release generation


## [v0.1.0] - 2024-01-24
### Added
- KiCi catalog component
