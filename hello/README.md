# Hello
A sample 555 timer LED blinky project to demonstrate Through Hole KiCAD
design and pipeline automation.

To view all project outputs of the
[master branch](https://ci-includes.gitlab.io/kici/hello/navigate/hello-navigate.html),
[interactive bill of materials](https://ci-includes.gitlab.io/kici/hello/ibom/hello-ibom.html).
or [KiCanvas](https://ci-includes.gitlab.io/kici/hello/kicanvas/hello-kicanvas.html) output.

For release files, see the
[releases page](https://gitlab.com/ci-includes/kici/-/releases) or the
[package repository](https://gitlab.com/ci-includes/kici/-/packages).

![PCB](https://ci-includes.gitlab.io/kici/hello/png/hello-3D_blender_front.png)
