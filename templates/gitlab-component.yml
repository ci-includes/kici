# SPDX-License-Identifier: AGPL-3.0-or-later
#
# Copyright (C) 2024 Olliver Schinagl <oliver@schinagl.nl>

spec:
  inputs:
    attach_diff:
      description: 'Attach the PDF rendered "diff" of a merge-request as a discussion thread.'
      default: 'false'
      options:
        - 'false'
        - 'true'
    kibot_ver:
      description: 'Version of KiBot container to use.'
      default: '1.8.2'
    kicad_ver:
      description: 'Version of KiCad to use.'
      default: '8'
    kicad_models_allow_failure:
      description: 'Allow failures when using non-local 3D model files in kicad_pcb files.'
      default: 'false'
      options:
        - 'false'
        - 'true'
    kicad_models_check_run:
      description: 'Check the kicad project files for non-local 3d model files.'
      default: 'true'
      options:
        - 'false'
        - 'true'
    projects:
      description: 'Space separated list of subdirectories to scan for KiCad files. If not supplied, the root is used.'
      default: './'
    skip_warnings_on:
      description: 'Comma seperated list of integers of warnings to ignore. E.g. "132,143"'
      default: ''
    stage_lint:
      description: 'The stage under which the linting jobs run as, may also be `build` or anything else.'
      default: 'lint'
    stage_build:
      description: 'The stage under which the build-ish jobs run as, may also be `lint` or anything else.'
      default: 'build'
    stage_deploy:
      description: 'The stage under which the package deploy is run as, defaults to `deploy`.'
      default: 'deploy'
    deploy_pages:
      description: 'Deploy the current state of master to GitLab pages.'
      default: 'always'
      options:
        - 'always'
        - 'never'
    diff-tag_match_regex:
      description: 'Regular expression to be used to match a diff-tag job.'
      default: '/^v\d+\.\d+.*$/'
    deploy_match_regex:
      description: 'Regular expression to be used to match a deploy job.'
      default: '/^v\d+\.\d+\.\d+.*$/'
    deploy_files:
      description: 'Deploy all individual files as a release, allowing for downloading of each file in a release.'
      default: 'true'
      options:
        - 'false'
        - 'true'
    deploy_tarball:
      description: 'Deploy a tarball of the files as a release archive, allowing for downloading of all files in a release in one click.'
      default: 'true'
      options:
        - 'false'
        - 'true'
---

variables:
  DEBUG_TRACE_CI:
    description: 'Enable debugging of kibot. This is loud and leaves large videos in some cases! Set to "kici" to also debug the pipeline.'

# Linting
# ===========================================================================
.kijob:
  image: 'ghcr.io/inti-cmnb/kicad$[[ inputs.kicad_ver ]]_auto_full:$[[ inputs.kibot_ver ]]'
  needs: []
  rules:
    - exists:
      - "**/.kibot/${CI_JOB_NAME}.kibot.yaml"
  variables:
    GIT_DEPTH: 1
    GIT_SUBMODULE_STRATEGY: recursive
    HOME: '/tmp'
    KICI_PROJECTS: $[[ inputs.projects ]]
    KICI_CI_ATTACH_DIFF: $[[ inputs.attach_diff ]]
    KICI_SKIP_WARNINGS_ON: $[[ inputs.skip_warnings_on ]]
  artifacts:
    paths:
      - "${CI_PROJECT_DIR}/reports/"
      - "${CI_PROJECT_DIR}/outputs/"
    expire_in: 1 week
    when: always
  before_script:
    - test "${DEBUG_TRACE_CI:-}" = 'kici' && set -x
    - kicad-cli --version
    - kibot --version
    - |
      HAVE_SSE41="$(grep -q 'sse4_1' '/proc/cpuinfo' && echo 'true' || echo 'false')"
      export HAVE_SSE41
    - |
      attach_diff_pdf()
      {
        unset _diff_sch
        unset _diff_pcb
        unset _diff_pdf_sch
        unset _diff_pdf_pcb
        if [ "${KICI_CI_ATTACH_DIFF:-}" != 'true' ] || \
           [ -z "${CI_MERGE_REQUEST_IID:-}" ] || \
           [ -z "${CI_MERGE_REQUEST_PROJECT_PATH:-}" ]; then
          return
        fi
        if [ "${KICI_CI_ATTACH_DIFF:-}" = 'true' ] &&
           [ -z "${KICI_CI_TOKEN:-${CI_PERSONAL_TOKEN:-}}" ]; then
          echo 'Diff comment attachment was requested, but no token was set, skipping. Please check the documentation.'
          return
        fi
        commit_version_shas="$(curl \
             --data '{"query":"query get_diff_refs{project(fullPath:\"'"${CI_MERGE_REQUEST_PROJECT_PATH}"'\"){mergeRequest(iid:\"'"${CI_MERGE_REQUEST_IID}"'\"){diffRefs{headSha}}}}"}' \
             --fail \
             --header "Content-Type: application/json" \
             --header "Authorization: Bearer ${KICI_CI_TOKEN:-${CI_PERSONAL_TOKEN}}" \
             --request 'POST' \
             --show-error \
             --silent \
             "${CI_API_GRAPHQL_URL}" | \
             tr '{},' '\n')"
        head_sha="$(echo "${commit_version_shas}" | sed -n 's|^"headSha":"\(.*\)"$|\1|p')"
        _diff_sch="$(find './' -maxdepth 1 -type 'f' -iname '*.kicad_sch' -printf '%P' -quit).pdf"
        _diff_pcb="$(find './' -maxdepth 1 -type 'f' -iname '*.kicad_pcb' -printf '%P' -quit).pdf"
        _artifact_prefix="${CI_JOB_URL}/artifacts/raw/outputs/${proj}/diff"
        _diff_pdf_sch="${_artifact_prefix}/$(find './outputs/diff' -maxdepth 1 -type 'f' -iname '*-diff_sch_*.pdf' -printf '%P' -quit)"
        _diff_pdf_pcb="${_artifact_prefix}/$(find './outputs/diff' -maxdepth 1 -type 'f' -iname '*-diff_pcb_*.pdf' -printf '%P' -quit)"
        KICI_DIFF_COMMENT="<h2>PDF diff for '<a href='${CI_MERGE_REQUEST_PROJECT_URL}/-/tree/${head_sha}/${proj}'>${proj}</a>'</h2>Commit: '${head_sha}'<br>Schematic: <a href='${_diff_pdf_sch}'>${_diff_sch}</a><br>PCB: <a href='${_diff_pdf_pcb}'>${_diff_pcb}</a>"
        curl \
             --data '{"query": "mutation attach_diff_pdf{createNote(input:{noteableId:\"gid://gitlab/MergeRequest/'"${CI_MERGE_REQUEST_ID}"'\" body:\"'"${KICI_DIFF_COMMENT:?Missing comment to post}"'\" mergeRequestDiffHeadSha:\"${head_sha}\"}){errors}}"}' \
             --fail \
             --header "Content-Type: application/json" \
             --header "Authorization: Bearer ${KICI_CI_TOKEN:-${CI_PERSONAL_TOKEN}}" \
             --request 'POST' \
             --show-error \
             --silent \
             --write-out "HTTP response: %{http_code}\n\n" \
             "${CI_API_GRAPHQL_URL}"
      }
    - |
      run_kibot_job()
      {
        _job="${1:?Missing argument to function}"
        _panel_job="${2:-}"
        if [ "${KIJOB_ATTACH_DIFF_PDF:-false}" != 'true' ]; then
          if [ "${KIJOB_DEF_PREV_MERGE:-false}" = 'true' ]; then
            _prev_sha="$(git log --max-count=1 --merges --pretty='format:%H')"
            if [ "${#_prev_sha}" -eq 40 ]; then
              GIT_DIFF_REF_OLD="${_prev_sha}"
              GIT_DIFF_REF_OLD_NAME='prev-merge'
            fi
          fi
          if [ "${KIJOB_DEF_PREV_TAG:-false}" = 'true' ]; then
            _prev_tag="$(git describe --abbrev=0 --tags HEAD~1)"
            GIT_DIFF_REF_OLD="${_prev_tag}"
            GIT_DIFF_REF_OLD_NAME="${_prev_tag}"
          fi
        fi
        for proj in ${KICI_PROJECTS:-./}; do
          if [ ! -f "${proj}/${_job}" ]; then
            continue
          fi
          _uuid="$(grep -B 1 Root "${proj}/"*'.kicad_pro' | sed -n 's|^[[:space:]]*\(\".*\"\),[[:space:]]*$|\1|p')"
          if ! _schematic="$(grep -l "(uuid ${_uuid})" "${proj}/"*'.kicad_sch' | sed "s|${proj}/||g")"; then
            echo "No root schematic with uuid '${_uuid}' detected in '${proj}', skipping."
            continue
          fi
          (
            set -eu
            cd "${proj}"
            if [ -s "${_panel_job}" ]; then
              _panel_path="$(sed -n -e "s|.*dir:[[:space:]]*'\(.*\)'$|\1|p" "${_panel_job}")"
              _panel_board="${_panel_path}/${_schematic%.kicad_sch}-panel.kicad_pcb"
              export _KIBOT_MANF_DIR_COMP="${_panel_path}"
              KICI_SKIP_WARNINGS_ON="75,135,164${KICI_SKIP_WARNINGS_ON:+",${KICI_SKIP_WARNINGS_ON}"}"
            fi
            echo "Running KiBot for '${proj}${_panel_board:+/${_panel_board}}' with schematic '${_schematic}' ..."
            mkdir -p "${CI_PROJECT_DIR}/reports/${proj}"
            if [ ! -d "reports" ]; then
              ln -s "${CI_PROJECT_DIR}/reports/${proj}" 'reports'
            fi
            mkdir -p "${CI_PROJECT_DIR}/outputs/${proj}"
            if [ ! -d "outputs" ]; then
              ln -s "${CI_PROJECT_DIR}/outputs/${proj}" 'outputs'
            fi
            # shellcheck source=/dev/null
            if [ -s '.kibot/kici_conf.env' ]; then
              . '.kibot/kici_conf.env'
              # shellcheck disable=SC2013  While subshell kills export
              for _kici_var in $(grep "^KICI_.*=.*$" '.kibot/kici_conf.env'); do
                export "${_kici_var%%'='*}"
              done
            fi
            if [ "${HAVE_SSE41:-false}" != 'true' ]; then
              KICI_BLENDER_NO_DENOISER='true';
              KICI_BLENDER_SAMPLES="$((${KICI_BLENDER_SAMPLES:-15} * 5))";
              export KICI_BLENDER_NO_DENOISER
              export KICI_BLENDER_SAMPLES
            fi
            if [ -n "${KIJOB_ATTACH_DIFF_PDF:-}" ]; then
              export GIT_DIFF_REF_NEW
              export GIT_DIFF_REF_NEW_NAME
              export GIT_DIFF_REF_NEW_TYPE
              export GIT_DIFF_REF_OLD
              export GIT_DIFF_REF_OLD_NAME
            fi
            test -s '.kibot/kici_info.kibot.yaml' && kibot --plot-config '.kibot/kici_info.kibot.yaml' --schematic "${_schematic}"
            KICI_SKIP_WARNINGS_ON="11,165${KICI_SKIP_WARNINGS_ON:+",${KICI_SKIP_WARNINGS_ON}"}"
            kibot \
                  ${DEBUG_TRACE_CI:+--verbose --verbose --verbose --verbose} \
                  ${_panel_board:+--board "${_panel_board}"} \
                  --defs-from-env \
                  ${KICI_SKIP_WARNINGS_ON:+--no-warn "${KICI_SKIP_WARNINGS_ON}"} \
                  --plot-config "${_job}" \
                  --schematic "${_schematic}" \
                  --stop-on-warnings
            ${KIJOB_ATTACH_DIFF_PDF:+attach_diff_pdf}
            if [ -s "${CI_PROJECT_DIR}/reports/${proj}/"*'.html' ]; then
              mkdir -p "${CI_PROJECT_DIR}/outputs/${proj}/reports"
              mv "${CI_PROJECT_DIR}/reports/${proj}/"*'.html' "${CI_PROJECT_DIR}/outputs/${proj}/reports/"
            fi
            if [ -s "${CI_PROJECT_DIR}/reports/${proj}/"*'.rpt' ]; then
              cat "${CI_PROJECT_DIR}/reports/${proj}/"*'.rpt'
            fi
            echo "Done running kibot for '${proj}'!"
          )
        done
      }
  after_script:
    - find . -empty -type d -delete
  script: run_kibot_job ".kibot/${CI_JOB_NAME}.kibot.yaml"

kici_erc:
  extends: .kijob
  stage: $[[ inputs.stage_lint ]]

kici_drc:
  extends: .kijob
  stage: $[[ inputs.stage_lint ]]

kici_diff-git:
  extends: .kijob
  stage: $[[ inputs.stage_lint ]]
  variables:
    GIT_DEPTH: 0
    CI_COMMIT_TAG_REGEX: $[[ inputs.diff-tag_match_regex ]]
    GIT_DIFF_REF_NEW_TYPE: 'git'
  rules:
    - if: '$CI_COMMIT_TAG =~ $CI_COMMIT_TAG_REGEX'
      exists:
        - ".kibot/${CI_JOB_NAME}.kibot.yaml"
      variables:
        GIT_DIFF_REF_NEW: $CI_COMMIT_TAG
        GIT_DIFF_REF_NEW_NAME: $CI_COMMIT_TAG
        KIJOB_ATTACH_DIFF_PDF: 'false'
        KIJOB_DEF_PREV_TAG: 'true'
    - if: '$CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH'
      exists:
        - ".kibot/${CI_JOB_NAME}.kibot.yaml"
      variables:
        GIT_DIFF_REF_NEW: $CI_COMMIT_SHA
        GIT_DIFF_REF_NEW_NAME: $CI_COMMIT_SHORT_SHA
        KIJOB_ATTACH_DIFF_PDF: 'false'
        KIJOB_DEF_PREV_MERGE: 'true'
    - if: ($CI_MERGE_REQUEST_IID && $CI_MERGE_REQUEST_TARGET_BRANCH_NAME && $CI_COMMIT_REF_NAME)
      exists:
        - ".kibot/${CI_JOB_NAME}.kibot.yaml"
      variables:
        GIT_DIFF_REF_NEW: remotes/origin/$CI_COMMIT_REF_NAME
        GIT_DIFF_REF_NEW_NAME: $CI_COMMIT_REF_SLUG
        GIT_DIFF_REF_OLD: remotes/origin/$CI_MERGE_REQUEST_TARGET_BRANCH_NAME
        GIT_DIFF_REF_OLD_NAME: $CI_MERGE_REQUEST_TARGET_BRANCH_NAME
        KIJOB_ATTACH_DIFF_PDF: 'true'

kici_models_check:
  image: "index.docker.io/library/alpine:latest"
  stage: $[[ inputs.stage_lint ]]
  needs: []
  variables:
    GIT_DEPTH: 0
    GIT_SUBMODULE_STRATEGY: "none"
    KICI_PROJECTS: $[[ inputs.projects ]]
    KICAD_MODELS_ALLOW_FAILURE: $[[ inputs.kicad_models_allow_failure ]]
    KICAD_MODELS_CHECK_RUN: $[[ inputs.kicad_models_check_run ]]
  rules:
    - if: '$KICAD_MODEL_CHECK_RUN == "false"'
      when: never
    - if: '$KICAD_MODEL_CHECK_ALLOW_FAILURE == "true"'
      allow_failure: true
    - exists:
      - "**/*.kicad_pcb"
      when: always
  artifacts:
    paths:
      - "${CI_PROJECT_DIR}/reports/"
    expire_in: 1 week
    when: always
  before_script:
    - mkdir -p "${CI_PROJECT_DIR}/reports"
  script:
    - |
      for _proj in ${KICI_PROJECTS:-./}; do
        if [ ! -d "${_proj}" ]; then
          continue
        fi
        mkdir -p "${CI_PROJECT_DIR}/reports/${_proj}"
        if find "${CI_PROJECT_DIR}/${_proj}" -iname '*.kicad_pcb' | xargs grep -H "^[[:space:]]*(model \"" | grep -v "\${KIPRJMOD}" 2>&1 | tee -a "${CI_PROJECT_DIR}/reports/${_proj}/kicad_models_check.log"; then
          _exit_code=1
        fi
      done
      exit "${_exit_code:-0}"

# Build
# ===========================================================================
kici_blender:
  extends: .kijob
  stage: $[[ inputs.stage_build ]]

kici_boardview:
  extends: .kijob
  stage: $[[ inputs.stage_build ]]

kici_bom:
  extends: .kijob
  stage: $[[ inputs.stage_build ]]

kici_design:
  extends: .kijob
  stage: $[[ inputs.stage_build ]]

kici_export:
  extends: .kijob
  stage: $[[ inputs.stage_build ]]

kici_fab:
  extends: .kijob
  stage: $[[ inputs.stage_build ]]

kici_gerbers:
  extends: .kijob
  stage: $[[ inputs.stage_build ]]

kici_ibom:
  extends: .kijob
  stage: $[[ inputs.stage_build ]]

kici_kicanvas:
  extends: .kijob
  stage: $[[ inputs.stage_build ]]

kici_panelize:
  extends: .kijob
  stage: $[[ inputs.stage_build ]]

kici_panelize_fab:
  extends: .kijob
  stage: $[[ inputs.stage_build ]]
  needs:
    - job: kici_panelize
      artifacts: true
      optional: false
  script: run_kibot_job ".kibot/${CI_JOB_NAME}.kibot.yaml" ".kibot/kici_panelize.kibot.yaml"

kici_pdf:
  extends: .kijob
  stage: $[[ inputs.stage_build ]]

kici_stencil:
  extends: .kijob
  stage: $[[ inputs.stage_build ]]

kici_navigate:
  extends: .kijob
  stage: $[[ inputs.stage_build ]]
  variables:
    GIT_DEPTH: 0
  needs:
    - job: kici_blender
      artifacts: true
      optional: true
    - job: kici_boardview
      artifacts: true
      optional: true
    - job: kici_bom
      artifacts: true
      optional: true
    - job: kici_export
      artifacts: true
      optional: true
    - job: kici_fab
      artifacts: true
      optional: true
    - job: kici_gerbers
      artifacts: true
      optional: true
    - job: kici_ibom
      artifacts: true
      optional: true
    - job: kici_kicanvas
      artifacts: true
      optional: true
    - job: kici_pdf
      artifacts: true
      optional: true
    - job: kici_render3d
      artifacts: true
      optional: true
    - job: kici_stencil
      artifacts: true
      optional: true
    - job: kici_step
      artifacts: true
      optional: true
    - job: kici_vrml
      artifacts: true
      optional: true

kici_present:
  extends: .kijob
  stage: $[[ inputs.stage_build ]]
  needs:
    - job: kici_gerbers
      artifacts: true
      optional: false
    - job: kici_blender
      artifacts: true
      optional: false

kici_step:
  extends: .kijob
  stage: $[[ inputs.stage_build ]]

kici_render3d:
  extends: .kijob
  stage: $[[ inputs.stage_build ]]

kici_vrml:
  extends: .kijob
  stage: $[[ inputs.stage_build ]]

kici_outputs:
  stage: $[[ inputs.stage_build ]]
  needs:
    - !reference [kici_navigate, needs]
    - job: kici_erc
      artifacts: true
      optional: true
    - job: kici_drc
      artifacts: true
      optional: true
    - job: kici_design
      artifacts: true
      optional: true
    - job: kici_diff-git
      artifacts: true
      optional: true
    - job: kici_panelize
      artifacts: true
      optional: true
    - job: kici_panelize_fab
      artifacts: true
      optional: true
    - job: kici_present
      artifacts: true
      optional: true
    - job: kici_navigate
      artifacts: true
      optional: true
  when: always
  variables:
    KICI_PROJECTS: $[[ inputs.projects ]]
    KICI_DEPLOY_FILES: $[[ inputs.deploy_files ]]
  artifacts:
    paths:
      - "${CI_PROJECT_DIR}/reports/"
      - "${CI_PROJECT_DIR}/outputs/"
      - "${CI_PROJECT_DIR}/public/"
    expire_in: 1 week
    when: always
  script:
    - |
      for _proj in ${KICI_PROJECTS:-./}; do
        if [ ! -d "${CI_PROJECT_DIR}/outputs/${_proj}" ]; then
          continue
        fi
        if [ -n "${KICI_PROJECTS##*' '*}" ]; then
          _proj_name="${CI_PROJECT_NAME}"
        else
          _proj_name="${_proj}"
        fi
        if [ "${KICI_DEPLOY_FILES:-true}" = 'true' ]; then
          for _kijob in 'export' 'kicanvas' 'navigate' 'present'; do
            if [ ! -d "${CI_PROJECT_DIR}/outputs/${_proj}/${_kijob}" ]; then
              continue
            fi
            echo "Archiving '${_proj_name}/${_kijob}' for multi-projects to avoid duplicate names."
            tar c -C "${CI_PROJECT_DIR}/outputs/${_proj}" -z -f "${CI_PROJECT_DIR}/outputs/${_proj}/${_proj_name}-${_kijob}.tar.gz" "${_kijob}/"
            rm -r "${CI_PROJECT_DIR}/outputs/${_proj}/${_kijob}"
          done
        fi
        (
          cd "${CI_PROJECT_DIR}/outputs/${_proj}"
          find "./" -type f ! -iname "${_proj_name}-filelist.txt" | cut -c3- > "${CI_PROJECT_DIR}/outputs/${_proj}/${_proj_name}-filelist.txt"
        )
      done
  after_script:
    -  echo 'Changing file permissions avoid access errors during deploy update.'
    -  chmod 'o+w' -R "${CI_PROJECT_DIR}/outputs/"

kici_deploy:
  image: "index.docker.io/curlimages/curl:latest"
  stage: $[[ inputs.stage_deploy ]]
  variables:
    KICI_PROJECTS: $[[ inputs.projects ]]
    KICI_DEPLOY_REGEX: $[[ inputs.deploy_match_regex ]]
    KICI_DEPLOY_FILES: $[[ inputs.deploy_files ]]
    KICI_DEPLOY_TARBALL: $[[ inputs.deploy_tarball ]]
  needs:
    - job: kici_outputs
      artifacts: true
      optional: false
  rules:
    - if: '$CI_COMMIT_TAG =~ $KICI_DEPLOY_REGEX'
  artifacts:
    paths:
      - "${CI_PROJECT_DIR}/assets/"
    expire_in: 1 week
    when: always
  before_script:
    - mkdir -p "${CI_PROJECT_DIR}/assets/"
    - printf '[' > "${CI_PROJECT_DIR}/assets/release.json"
  after_script:
    - printf ']' >> "${CI_PROJECT_DIR}/assets/release.json"
  script:
    - |
      _first_json_entry='true'
      for _proj in ${KICI_PROJECTS:-./}; do
        if [ ! -d "${CI_PROJECT_DIR}/outputs/${_proj}" ]; then
          continue
        fi
        if [ -n "${KICI_PROJECTS##*' '*}" ]; then
          _proj_name="${CI_PROJECT_NAME}"
        else
          _proj_name="${_proj}"
        fi
        if [ "${KICI_DEPLOY_TARBALL:-true}" = 'true' ]; then
          echo "Creating tarball for '${_proj_name}' ..."
          tar c -C "${CI_PROJECT_DIR}/outputs" -z -f "${CI_PROJECT_DIR}/outputs/${_proj}/${_proj_name}.tar.gz" "${_proj}"
          _upload_files="${CI_PROJECT_DIR}/outputs/${_proj}/${_proj_name}.tar.gz"
        fi
        echo "Generating packages for '${_proj_name}' ..."
        if [ "${KICI_DEPLOY_FILES:-true}" = 'true' ]; then
          _upload_files="$(find "${CI_PROJECT_DIR}/outputs/${_proj}" -type f -print)"
        fi
        for _upload_file in ${_upload_files}; do
          _local_part="${_upload_file##*"${CI_PROJECT_DIR}/outputs/${_proj}/"}"
          _output_type="${_local_part%'/'*}"
          _file_path="$(echo "${_local_part}" | tr '/' '-')"
          _file_name="${_file_path}"
          _url="${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/generic/${_proj_name}/${CI_COMMIT_TAG#v}/${_file_name}"
          curl \
               --fail \
               --header 'Content-Type: application/json' \
               --header "Job-Token: ${CI_JOB_TOKEN}" \
               --output '/dev/null' \
               --request 'PUT' \
               --retry 3 \
               --show-error \
               --silent \
               --upload-file "${_upload_file}" \
               --write-out "Uploaded file: '${_upload_file}'; HTTP response: %{http_code}\n\n" \
               "${_url}"
          if [ "${_first_json_entry:-false}" != 'true' ]; then
            printf ',' >> "${CI_PROJECT_DIR}/assets/release.json"
          else
            _first_json_entry='false'
          fi
          printf "{\"name\":\"%s\",\"url\":\"%s\",\"filepath\":\"/%s\"}" "${_file_name}" "${_url}" "${_file_path}" >> "${CI_PROJECT_DIR}/assets/release.json"
          sed -i "s|^\(${_local_part}\)$|\1\t${_file_path}|p" "${CI_PROJECT_DIR}/outputs/${_proj}/${_proj_name}-filelist.txt"
        done
      done
    - echo 'Subdirectories where lost due to gitlab-org/gitlab#439530.'

pages:
  stage: $[[ inputs.stage_deploy ]]
  variables:
    KICI_PROJECTS: $[[ inputs.projects ]]
  needs:
    - job: kici_outputs
      artifacts: true
      optional: false
  rules:
    - if: '$CI_COMMIT_REF_NAME == $CI_DEFAULT_BRANCH'
      when: $[[ inputs.deploy_pages ]]
  artifacts:
    paths:
      - "${CI_PROJECT_DIR}/public"
    expire_in: '1 day'
  before_script:
    - cp -a -T "${CI_PROJECT_DIR}/outputs/" "${CI_PROJECT_DIR}/public/"
  script:
    - |
      echo '<html>' > "${CI_PROJECT_DIR}/public/index.html"
      for _proj in ${KICI_PROJECTS:-./}; do
        if [ ! -d "${CI_PROJECT_DIR}/public/${_proj}" ]; then
          continue
        fi
        if [ -n "${KICI_PROJECTS##*' '*}" ]; then
          _proj_name="${CI_PROJECT_NAME}"
        else
          _proj_name="${_proj}"
        fi
        for _archive in "${CI_PROJECT_DIR}/public/${_proj}/"*'.tar.gz'; do
          if [ -s "${_archive}" ]; then
            tar x -C "${CI_PROJECT_DIR}/public/${_proj}" -f "${_archive}"
            rm "${_archive}"
          fi
        done
        if [ -s "${CI_PROJECT_DIR}/public/${_proj}/present/index.html" ]; then
          _content='present/index.html'
        elif [ -d "${CI_PROJECT_DIR}/public/${_proj}/navigate/" ]; then
          _content="navigate/$(basename "$(find "${CI_PROJECT_DIR}/public/${_proj}/navigate/" -maxdepth 1 -type 'f' -iname '*-navigate.html' -print -quit)")"
        elif [ -d "${CI_PROJECT_DIR}/public/${_proj}/kicanvas/" ]; then
          _content="ibom/$(basename "$(find "${CI_PROJECT_DIR}/public/${_proj}/kicanvas/" -maxdepth 1 -type 'f' -iname '*-kicanvas.html' -print -quit)")"
        elif [ -d "${CI_PROJECT_DIR}/public/${_proj}/ibom/" ]; then
          _content="ibom/$(basename "$(find "${CI_PROJECT_DIR}/public/${_proj}/ibom/" -maxdepth 1 -type 'f' -iname '*-ibom.html' -print -quit)")"
        fi
        _body="${_body:-}<h2><center><a href='${_proj}/${_content:-}'>${_proj_name}</a></center></h2>\n"
      done
      if [ -n "${KICI_PROJECTS##*' '*}" ]; then
        printf '<head><meta http-equiv="refresh" content="0; %s"/></head>\n' "${_content:-.}" >> "${CI_PROJECT_DIR}/public/index.html"
      fi
      # shellcheck disable=SC2059  # Interpret \n from result variable
      printf "<body>\n${_body:-}</body>\n" >> "${CI_PROJECT_DIR}/public/index.html"
      echo '</html>' >> "${CI_PROJECT_DIR}/public/index.html"
      echo "Deployed pages to: ${CI_PAGES_URL}/index.html"
